import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/servises/users.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$;

  constructor(private usersService: UsersService) {}


  ngOnInit(): void {
  }

}
