import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsersService } from 'src/app/servises/users.service';
import { PostDialogComponent } from './post-dialog/post-dialog.component';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$;

  constructor(private usersService: UsersService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }
  opendialog() {
    this.dialog.open(PostDialogComponent, {
      width: '800px',
      height: '600px',
      panelClass: 'dailogC',
    })
  }


}
