import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/servises/users.service';

@Component({
  selector: 'app-post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.css']
})
export class PostDialogComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$;

  constructor(private usersService: UsersService,) { }


  ngOnInit(): void {
  }

}
