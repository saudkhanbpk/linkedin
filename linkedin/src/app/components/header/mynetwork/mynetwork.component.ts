import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/servises/users.service';
import { JobsComponent } from '../jobs/jobs.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-mynetwork',
  templateUrl: './mynetwork.component.html',
  styleUrls: ['./mynetwork.component.css']
})
export class MynetworkComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$;

  constructor(private usersService: UsersService,
    private dialog: MatDialog
    ) {}

  ngOnInit(): void {
  }
  
 

}
