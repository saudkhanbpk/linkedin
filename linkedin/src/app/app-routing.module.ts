import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/header/home/home.component';
import { JobsComponent } from './components/header/jobs/jobs.component';
import { ResumeComponent } from './components/header/jobs/resume/resume.component';
import { MessageComponent } from './components/header/message/message.component';
import { MynetworkComponent } from './components/header/mynetwork/mynetwork.component';
import { NotificationComponent } from './components/header/notification/notification.component';


import {
  canActivate,
  redirectLoggedInTo,
  redirectUnauthorizedTo,
} from '@angular/fire/auth-guard';
import { LoginComponent } from './components/auth/login/login.component';
import { SignUpComponent } from './components/auth/sign-up/sign-up.component';
import { ProfileComponent } from './components/auth/profile/profile.component';
import { FeedComponent } from './components/header/home/feed/feed.component';
import { UserProfileComponent } from './components/header/home/user-profile/user-profile.component';
import { PostDialogComponent } from './components/header/home/feed/post-dialog/post-dialog.component';


const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToHome = () => redirectLoggedInTo(['home']);
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LoginComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    ...canActivate(redirectLoggedInToHome),
  }, {
    path: 'profile',
    component: ProfileComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'home',
    component: HomeComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'mynetwork',
    component: MynetworkComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'job',
    component: JobsComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'messaging',
    component: MessageComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'notification',
    component: NotificationComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'cv',
    component: ResumeComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'feed',
    component: FeedComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'user-profile',
    component: UserProfileComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'post-dialog',
    component: PostDialogComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
