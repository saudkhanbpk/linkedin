// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: "AIzaSyAV4sbZHmwVAoWlAm1Am5ZWhtDU1QsTNzY",
    authDomain: "angular-linkedin-2e280.firebaseapp.com",
    databaseURL: "https://angular-linkedin-2e280-default-rtdb.firebaseio.com",
    projectId: "angular-linkedin-2e280",
    storageBucket: "angular-linkedin-2e280.appspot.com",
    messagingSenderId: "488265068344",
    appId: "1:488265068344:web:34f7eb032d86f143f25bcd"
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
